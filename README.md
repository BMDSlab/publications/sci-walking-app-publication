# A comparison of machine learning and clinical experts in predicting walking ability after Spinal Cord Injury

This repository contains all code for the analysis presented in the manuscript with the above title. Code was written and reviewed by Julia Bugajska and Louis P. Lukas.

Data can only be made available upon reasonable request to the corresponding author. Written proposals will be evaluated by the authors and EMSCI steering board, who will render a decision regarding suitability and appropriateness of the use of data. Approval of all authors and EMSCI steering board will be required and a data-sharing agreement must be signed before any data is shared.

**Python** and **package versions** used for the analysis can be inferred from `pyproject.toml`. All scripts for analysis, evaluation of results and plotting can be found in `sci_walking_app/`. Scripts can be executed independently once relevant paths to data have been provided.
