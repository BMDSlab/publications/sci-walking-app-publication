"""Aggregate user predictions collected via app and compute performance metrics.

Authors: Julia Bugajska, Louis Lukas
"""

from pathlib import Path

import pandas as pd
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    precision_score,
    recall_score,
    roc_auc_score,
)

DATA_PATH = Path("")
PREDICTION_THRESHOLD = 0.5


def _return_prob_yes(answers):
    """Return probability to predict "Yes"/walking for patient."""
    assert not answers.isna().any()
    try:
        return answers.value_counts()["Yes"] / answers.shape[0]
    except KeyError:
        # indicates no Yes answer provided by any user
        return 0


def _check_unanimous(answers):
    """Return if doctors unanimously agree on *correct* label."""
    user_answers = answers[answers.index.str.startswith("answer_user")]
    if answers["true_label"]:  # true label is walking
        if user_answers.all():  # and all doctors (correctly) predicted walking...
            return 1  # ...model predicts walking
        # otherwise at least one doctor predicted not walking...
        return 0  # ...and model predicts not walking
    # in case true label is not walking
    if user_answers.any():  # and at least one doctor predicted walking...
        return 1  # ...model predicts walking
    # otherwise all doctors (correctly) predicted not walking...
    return 0  # ...model predicts not walking


if __name__ == "__main__":
    user_predictions_long = pd.read_csv(
        DATA_PATH / "user-predictions" / "user-predictions-combined.csv", index_col=0
    )
    y_raw = pd.read_csv(DATA_PATH / "Y_values_AISnan_1812.csv", index_col=0)
    y = y_raw["SCIM2_12"]
    # ----------------------------------------------------------------------------------
    # evaluation of individual physicians
    performance_individual = pd.DataFrame(
        index=pd.MultiIndex.from_product(
            [["overall", "AIS_A", "AIS_B", "AIS_C", "AIS_D"], list(range(6, 12))]
        ),
        columns=[
            "accuracy",
            "precision",
            "recall",
            "specificity",
            "roc_auc",
        ],
    )
    performance_individual.index.names = ["subset", "user_id"]
    for expert in user_predictions_long["user_id"].unique():
        expert_predictions = user_predictions_long[
            user_predictions_long["user_id"] == expert
        ][["Patientennummer", "useranswer"]]
        expert_predictions["useranswer"] = expert_predictions["useranswer"].apply(
            lambda x: 1 if x == "Yes" else 0
        )
        tn, fp, fn, tp = confusion_matrix(y, expert_predictions["useranswer"]).ravel()
        performance_individual.loc[pd.IndexSlice["overall", expert], "accuracy"] = (
            accuracy_score(y, expert_predictions["useranswer"])
        )
        performance_individual.loc[pd.IndexSlice["overall", expert], "precision"] = (
            precision_score(y, expert_predictions["useranswer"])
        )
        performance_individual.loc[pd.IndexSlice["overall", expert], "recall"] = (
            recall_score(y, expert_predictions["useranswer"])
        )
        performance_individual.loc[pd.IndexSlice["overall", expert], "specificity"] = (
            tn / (tn + fp)
        )
        expert_predictions = expert_predictions.join(
            pd.read_csv(
                DATA_PATH / "X_values_non_summarized_AISnan_1812.csv", index_col=0
            )["AIS"],
            on="Patientennummer",
            how="left",
        )
        for ais in expert_predictions["AIS"].unique():
            out_sub = expert_predictions[expert_predictions["AIS"] == ais]
            y_sub = y[out_sub["Patientennummer"]]
            tn, fp, fn, tp = confusion_matrix(y_sub, out_sub["useranswer"]).ravel()
            test_specificity_by_ais = tn / (tn + fp)
            performance_individual.loc[
                pd.IndexSlice[f"AIS_{ais}", expert], "accuracy"
            ] = accuracy_score(y_sub, out_sub["useranswer"])
            performance_individual.loc[
                pd.IndexSlice[f"AIS_{ais}", expert], "precision"
            ] = precision_score(y_sub, out_sub["useranswer"])
            performance_individual.loc[
                pd.IndexSlice[f"AIS_{ais}", expert], "recall"
            ] = recall_score(y_sub, out_sub["useranswer"])
            performance_individual.loc[
                pd.IndexSlice[f"AIS_{ais}", expert], "specificity"
            ] = test_specificity_by_ais

    performance_individual.to_csv(DATA_PATH / "user_individual_performance.csv")
    performance_individual.reset_index().groupby("subset")[
        ["accuracy", "precision", "recall", "specificity"]
    ].mean().to_csv(DATA_PATH / "user_individual_performance_mean.csv")
    performance_individual.reset_index().groupby("subset")[
        ["accuracy", "precision", "recall", "specificity"]
    ].std().to_csv(DATA_PATH / "user_individual_performance_std.csv")
    # ----------------------------------------------------------------------------------
    # predictions based on simple majority
    user_predictions_aggregated = pd.DataFrame(
        index=user_predictions_long["Patientennummer"].unique().sort(),
        columns=["proba", "label", "correct", "AIS"],
    )
    user_predictions_aggregated["proba"] = user_predictions_long.groupby(
        "Patientennummer"
    )["useranswer"].aggregate(_return_prob_yes)
    user_predictions_aggregated["label"] = (
        user_predictions_aggregated["proba"] >= PREDICTION_THRESHOLD
    ).astype("bool")
    user_predictions_aggregated["correct"] = (
        user_predictions_aggregated["label"] == y
    ).astype("bool")
    user_predictions_aggregated["AIS"] = pd.read_csv(
        DATA_PATH / "X_values_non_summarized_AISnan_1812.csv", index_col=0
    )["AIS"]
    user_predictions_aggregated.to_csv(DATA_PATH / "user_majority_predictions.csv")
    performance_ensemble = pd.DataFrame(
        index=["overall", "AIS_A", "AIS_B", "AIS_C", "AIS_D"],
        columns=[
            "accuracy",
            "precision",
            "recall",
            "specificity",
            "roc_auc",
        ],
    )
    tn, fp, fn, tp = confusion_matrix(y, user_predictions_aggregated["label"]).ravel()
    performance_ensemble.loc["overall", "accuracy"] = accuracy_score(
        y, user_predictions_aggregated["label"]
    )
    performance_ensemble.loc["overall", "precision"] = precision_score(
        y, user_predictions_aggregated["label"]
    )
    performance_ensemble.loc["overall", "recall"] = recall_score(
        y, user_predictions_aggregated["label"]
    )
    performance_ensemble.loc["overall", "specificity"] = tn / (tn + fp)
    performance_ensemble.loc["overall", "roc_auc"] = roc_auc_score(
        y, user_predictions_aggregated["proba"]
    )
    for ais in user_predictions_aggregated["AIS"].unique():
        out_sub = user_predictions_aggregated[user_predictions_aggregated["AIS"] == ais]
        y_sub = y[out_sub.index]
        tn, fp, fn, tp = confusion_matrix(y_sub, out_sub["label"]).ravel()
        test_specificity_by_ais = tn / (tn + fp)
        performance_ensemble.loc[f"AIS_{ais}", "accuracy"] = accuracy_score(
            y_sub, out_sub["label"]
        )
        performance_ensemble.loc[f"AIS_{ais}", "precision"] = precision_score(
            y_sub, out_sub["label"]
        )
        performance_ensemble.loc[f"AIS_{ais}", "recall"] = recall_score(
            y_sub, out_sub["label"]
        )
        performance_ensemble.loc[f"AIS_{ais}", "specificity"] = test_specificity_by_ais
        performance_ensemble.loc[f"AIS_{ais}", "roc_auc"] = roc_auc_score(
            y_sub, out_sub["proba"]
        )
    performance_ensemble.to_csv(DATA_PATH / "user_majority_performance.csv")
    # ----------------------------------------------------------------------------------
    # predictions based on (correct) unanimous vote
    user_predictions_wide = user_predictions_long.set_index(
        ["Patientennummer", "user_id"]
    ).unstack(level=-1)["useranswer"]
    user_predictions_wide.columns.name = None
    user_predictions_wide.rename(
        lambda x: f"answer_user_{x}", axis="columns", inplace=True
    )
    user_predictions_wide = user_predictions_wide.apply(lambda x: x == "Yes")
    user_predictions_wide["true_label"] = y.astype("bool")
    user_predictions_wide["proba"] = user_predictions_wide.apply(
        _check_unanimous, axis=1
    )
    user_predictions_wide["label"] = (
        user_predictions_wide["proba"] >= PREDICTION_THRESHOLD
    ).astype("bool")
    user_predictions_wide["correct"] = (user_predictions_wide["label"] == y).astype(
        "bool"
    )
    user_predictions_wide["AIS"] = user_predictions_aggregated["AIS"]
    user_predictions_wide.to_csv(DATA_PATH / "user_unanimous_predictions.csv")

    performance_unanimous = pd.DataFrame(
        index=["overall", "AIS_A", "AIS_B", "AIS_C", "AIS_D"],
        columns=[
            "accuracy",
            "precision",
            "recall",
            "specificity",
            "roc_auc",
        ],
    )
    tn, fp, fn, tp = confusion_matrix(y, user_predictions_wide["label"]).ravel()
    performance_unanimous.loc["overall", "accuracy"] = accuracy_score(
        y, user_predictions_wide["label"]
    )
    performance_unanimous.loc["overall", "precision"] = precision_score(
        y, user_predictions_wide["label"]
    )
    performance_unanimous.loc["overall", "recall"] = recall_score(
        y, user_predictions_wide["label"]
    )
    performance_unanimous.loc["overall", "specificity"] = tn / (tn + fp)
    performance_unanimous.loc["overall", "roc_auc"] = roc_auc_score(
        y, user_predictions_wide["proba"]
    )
    for ais in user_predictions_wide["AIS"].unique():
        out_sub = user_predictions_wide[user_predictions_wide["AIS"] == ais]
        y_sub = y[out_sub.index]
        tn, fp, fn, tp = confusion_matrix(y_sub, out_sub["label"]).ravel()
        test_specificity_by_ais = tn / (tn + fp)
        performance_unanimous.loc[f"AIS_{ais}", "accuracy"] = accuracy_score(
            y_sub, out_sub["label"]
        )
        performance_unanimous.loc[f"AIS_{ais}", "precision"] = precision_score(
            y_sub, out_sub["label"]
        )
        performance_unanimous.loc[f"AIS_{ais}", "recall"] = recall_score(
            y_sub, out_sub["label"]
        )
        performance_unanimous.loc[f"AIS_{ais}", "specificity"] = test_specificity_by_ais
        performance_unanimous.loc[f"AIS_{ais}", "roc_auc"] = roc_auc_score(
            y_sub, out_sub["proba"]
        )
    performance_unanimous.to_csv(
        DATA_PATH / "results" / "full-cohort" / "user_unanimous_performance.csv"
    )
