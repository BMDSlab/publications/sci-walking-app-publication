"""Generate boxplots showing performance of individual doctors in comparison with vm LR.

Authors: Julia Bugajska, Louis Lukas
"""

from itertools import combinations
from pathlib import Path
import pandas as pd

from compare_auc_delong_xu import delong_roc_test

DATA_PATH = Path("")

if __name__ == "__main__":
    y = pd.read_csv(DATA_PATH / "Y_values_AISnan_1812.csv", index_col=0)["SCIM2_12"]
    model_prediction_files = {
        "all-feature LR": "lr_test_set_predictions.csv",
        "physician ensemble": "user_majority_predictions.csv",
        "vM lr": "vmlr_predictions.csv",
        "xgboost": "xgboost_test_set_predictions.csv",
    }
    model_predictions = {}
    results = pd.DataFrame(  # pylint: disable=C0103
        index=pd.MultiIndex.from_tuples(combinations(model_prediction_files.keys(), 2)),
        columns=["p-value"],
    ).sort_index(ascending=False)

    for model1, model2 in results.index:
        if model1 not in model_predictions:
            model_predictions[model1] = pd.read_csv(
                DATA_PATH / model_prediction_files[model1], index_col=0
            )
        if model2 not in model_predictions:
            model_predictions[model2] = pd.read_csv(
                DATA_PATH / model_prediction_files[model2], index_col=0
            )

        log_p = delong_roc_test(
            y, model_predictions[model1]["proba"], model_predictions[model2]["proba"]
        )[0][0]
        results.loc[pd.IndexSlice[model1, model2], "p-value"] = 10**log_p

    results.sort_index(inplace=True)
    results.to_csv(DATA_PATH / "results" / "delong-roc-auc-overall.csv")
