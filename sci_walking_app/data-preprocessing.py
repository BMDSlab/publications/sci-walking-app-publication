"""Clean and subset data for analysis.

@author: Julia Bugajska, Louis Lukas
"""

import warnings
from pathlib import Path

import pandas as pd
import numpy as np

import sci_walking_app.definitions as definitions

DATA_PATH = Path("")
RESULTS_PATH = Path("")


def convert_scim_score_to_binary(score):
    """Convert SCIM subitem 12 score to binary label (non-walker / walker) based on
    criterion used in van Middendorp et al. (2011)."""
    if score <= 3:
        return 0
    else:
        return 1


def convert_age_to_binary(age):
    """Convert age to binary feature based on criterion used in
    van Middendorp et al. (2011)."""
    if age <= 65:
        return 0
    else:
        return 1


def convert_dap_vac(dap_vac):
    """Convert DAP from string to binary label."""
    if dap_vac.lower() == "yes":
        return 1
    else:
        return 0


if __name__ == "__main__":
    warnings.simplefilter(action="ignore", category=pd.errors.DtypeWarning)
    # https://stackoverflow.com/questions/68292862/performancewarning-dataframe-is-highly-fragmented-this-is-usually-the-result-o
    warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)
    emsci = pd.read_csv(DATA_PATH / "emsci_data_2020.csv")
    emsci_grouped_by_patients = emsci.groupby("Patientennummer")
    print(f"Number of patients: {emsci_grouped_by_patients.ngroups}")
    print("#" * 5 + " filtering for traumatic injuries " + "#" * 5)
    emsci_traumatic = emsci.drop(emsci[emsci.Cause != "traumatic"].index)
    emsci_traumatic_grouped_by_patients = emsci_traumatic.groupby("Patientennummer")
    print(f"Number of patients: {emsci_traumatic_grouped_by_patients.ngroups}")
    print("#" * 5 + " filtering for patients younger than 18 yrs " + "#" * 5)
    emsci_traumatic_adult = emsci_traumatic.drop(
        emsci_traumatic[emsci_traumatic.AgeAtDOI < 18].index
    )
    emsci_traumatic_adult_grouped_by_patients = emsci_traumatic_adult.groupby(
        "Patientennummer"
    )
    print(f"Number of patients: {emsci_traumatic_adult_grouped_by_patients.ngroups}")
    # NT ~ not testable; treat as NaN
    # 5* ~ condition other than SCI present; treat as 5
    emsci_traumatic_adult.replace("NT", np.nan, inplace=True)
    emsci_traumatic_adult.replace("5*", 5, inplace=True)
    print("#" * 5 + " filtering for missing values " + "#" * 5)
    emsci_traumatic_na_removed = emsci_traumatic_adult.dropna(
        subset=definitions.MOTOR_SENSORY_COLUMNS
    )
    emsci_traumatic_na_removed = emsci_traumatic_na_removed.dropna(subset=["SCIM2_12"])
    emsci_traumatic_na_removed_gouped_by_patients = emsci_traumatic_na_removed.groupby(
        "Patientennummer"
    )
    print(
        f"Number of patients: {emsci_traumatic_na_removed_gouped_by_patients.ngroups}"
    )
    emsci_traumatic_na_removed[definitions.MOTOR_SENSORY_COLUMNS] = (
        emsci_traumatic_na_removed[definitions.MOTOR_SENSORY_COLUMNS].astype(int)
    )
    print("#" * 5 + " filtering for weeks (2 or 4) and (26 or 52) available " + "#" * 5)
    emsci_traumatic_na_removed_early_late = (
        emsci_traumatic_na_removed_gouped_by_patients.filter(
            lambda x: (
                (x["ExamStage_weeks"].max() == 52 or x["ExamStage_weeks"].max() == 26)
                and (x["ExamStage_weeks"].min() == 4 or x["ExamStage_weeks"].min() == 2)
            )
        )
    )
    print(
        "Number of patients: "
        + f"{emsci_traumatic_na_removed_early_late.groupby('Patientennummer').ngroups}"
    )
    emsci_traumatic_na_removed_early_late["SCIM2_12"] = (
        emsci_traumatic_na_removed_early_late["SCIM2_12"].apply(
            convert_scim_score_to_binary
        )
    )
    emsci_traumatic_na_removed_early_late["AgeAtDOI_binarised"] = (
        emsci_traumatic_na_removed_early_late["AgeAtDOI"].apply(convert_age_to_binary)
    )
    emsci_traumatic_na_removed_early_late["DAP"] = (
        emsci_traumatic_na_removed_early_late["DAP"].astype("str")
    )
    emsci_traumatic_na_removed_early_late["DAP"] = (
        emsci_traumatic_na_removed_early_late["DAP"].apply(convert_dap_vac)
    )
    emsci_traumatic_na_removed_early_late["VAC"] = (
        emsci_traumatic_na_removed_early_late["VAC"].astype("str")
    )
    emsci_traumatic_na_removed_early_late["VAC"] = (
        emsci_traumatic_na_removed_early_late["VAC"].apply(convert_dap_vac)
    )

    # @Julia: definition of motor and sensory columns according to Methods from
    # van Middendorp (max. of left/right) left out as this is never written to disk
    # or used otherwise

    columns_to_keep = [
        "Patientennummer",
        "ExamStage_weeks",
        "Sexcd",
        "AgeAtDOI",
        "AgeAtDOI_binarised",
        "AIS",
        "SCIM2_12",
    ] + definitions.MOTOR_SENSORY_COLUMNS
    emsci_to_analyse = emsci_traumatic_na_removed_early_late[columns_to_keep].copy(
        deep=True
    )
    emsci_to_analyse["SCIM2_12"] = emsci_to_analyse["SCIM2_12"].astype("category")
    emsci_to_analyse["Sexcd"] = emsci_to_analyse["Sexcd"].astype("category")

    Y = emsci_to_analyse[["Patientennummer", "ExamStage_weeks", "SCIM2_12"]]
    Y_wk52 = Y.groupby("Patientennummer").filter(
        lambda x: (x["ExamStage_weeks"].max() == 52)
    )
    Y_wk52.drop(Y_wk52[Y_wk52.ExamStage_weeks != 52].index, inplace=True)
    Y_wk26 = Y.groupby("Patientennummer").filter(
        lambda x: (x["ExamStage_weeks"].max() == 26)
    )
    Y_wk26.drop(Y_wk26[Y_wk26.ExamStage_weeks != 26].index, inplace=True)
    Y = pd.concat([Y_wk26, Y_wk52])
    Y.sort_values("Patientennummer", inplace=True)
    Y.reset_index(drop=True, inplace=True)

    X = emsci_to_analyse.copy(deep=True)
    X.drop(columns=["SCIM2_12"], inplace=True)
    X_wk4 = X.groupby("Patientennummer").filter(
        lambda x: (x["ExamStage_weeks"].min() == 4)
    )
    X_wk4.drop(X_wk4[X_wk4.ExamStage_weeks != 4].index, inplace=True)
    X_wk2 = X.groupby("Patientennummer").filter(
        lambda x: (x["ExamStage_weeks"].min() == 2)
    )
    X_wk2.drop(X_wk2[X_wk2.ExamStage_weeks != 2].index, inplace=True)
    X = pd.concat([X_wk4, X_wk2])
    # X.drop("ExamStage_weeks", axis=1, inplace=True)
    X.sort_values("Patientennummer", inplace=True)
    X.reset_index(drop=True, inplace=True)

    # as AIS grade included in dataframes to be used for analysis (...for stratified
    # analysis of results etc), need to check for NAs there as well
    print("#" * 5 + " filtering for missing AIS grade " + "#" * 5)
    ais_na_idxs = X[X["AIS"].isna()].index.tolist()
    X.drop(ais_na_idxs, inplace=True, errors="ignore")
    Y.drop(ais_na_idxs, inplace=True, errors="ignore")
    print(f"Number of patients: {X.shape[0]}")

    X.to_csv(RESULTS_PATH / "X_values_non_summarized_AISnan_1812.csv", index=False)
    Y.to_csv(RESULTS_PATH / "Y_values_AISnan_1812.csv", index=False)
