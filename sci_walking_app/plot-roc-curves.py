"""Plot ROC curves for all models.

Authors: Julia Bugajska, Louis Lukas
"""

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import roc_curve, roc_auc_score

DATA_PATH = Path("")
PLOTS_PATH = Path("")
COLOURS = {
    # https://www.color-hex.com/color-palette/1047605
    # https://www.color-hex.com/color-palette/1047606
    "all-feature LR": "#cf0072",
    "XGBoost": "#4f2d7f",
    "Physician Ensemble": "#3f9c35",
    "vM LR": "#0094b3",
    # repeat colours for AIS grades
    "A": "#cf0072",
    "B": "#4f2d7f",
    "C": "#3f9c35",
    "D": "#0094b3",
}


def plot_roc_curve(
    ax: plt.axis,  # pylint: disable=W0621
    fpr_tpr_auc_dict: dict,  # pylint: disable=W0621
    add_title: bool = False,
    add_legend: bool = False,
    add_random: bool = False,
):
    """Plot ROC curve for one or multiple models.

    ax : matplotlib.pyplot.axis
        Axis instance on which to plot histogram of probabilities.
    fpr_tpr_auc_dict : dict
        Nested dictionary; top-level keys are model name(s); second level is dictionary
        w/ fpr, tpr, auc as keys and the corresponding values.
    add_title : bool
        Flag indicating whether title should be added; if True, use top level key of
        fpr_tpr_auc_dict and AUC value for title. Only meant to be used with single
        model.
    add_legend : bool
        Flag indicating whether legend should be added; if True, use top level keys of
        fpr_tpr_auc_dict and AUC value for legend.
    add_random : bool
        Flag indicating whether a dashed line on the main diagonal should be added to
        indicate the performance of a random classifier.
    """
    fontsize_label = 20
    fontsize_legend = 16
    fontsize_ticks = 16
    fontsize_title = 20

    for model, metrics in fpr_tpr_auc_dict.items():  # pylint: disable=W0621
        ax.plot(
            metrics["fpr"],
            metrics["tpr"],
            label=f"{model} (AUC={metrics['auc']:.2f})",
            color=COLOURS[model],
            ls="-" if len(fpr_tpr_auc_dict) == 1 else "-.",
            lw=2 if len(fpr_tpr_auc_dict) == 1 else 1.5,
            alpha=1 if len(fpr_tpr_auc_dict) == 1 else 0.8,
        )

    if add_random:
        ax.plot(
            np.arange(0, 1.05, 0.2),
            np.arange(0, 1.05, 0.2),
            label="random classifier (AUC=0.5)",
            color="k",
            ls="--",
            lw=1,
        )

    if add_title:
        assert len(fpr_tpr_auc_dict) == 1
        ax.set_title(
            "ROC curve "
            + model  # pylint: disable=W0631
            + f"\n(AUC={metrics['auc']:.2f})",  # pylint: disable=W0631
            fontsize=fontsize_title,
            fontweight="bold",
        )
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_xticks(
        np.arange(0, 1.05, 0.2),
        [f"{i:.1f}" for i in np.arange(0, 1.05, 0.2)],
        fontsize=fontsize_ticks,
    )
    ax.set_yticks(
        np.arange(0, 1.05, 0.2),
        [f"{i:.1f}" for i in np.arange(0, 1.05, 0.2)],
        fontsize=fontsize_ticks,
    )
    ax.set_xlabel("FPR", fontsize=fontsize_label)
    ax.set_ylabel("TPR", fontsize=fontsize_label)
    if add_legend:
        ax.legend(loc="lower right", fontsize=fontsize_legend)

    return ax


if __name__ == "__main__":
    plt.rcParams.update({"font.family": "serif", "font.serif": ["Times New Roman"]})
    model_predictions = {
        "all-feature LR": "lr_test_set_predictions.csv",
        "XGBoost": "xgboost_test_set_predictions.csv",
        "Physician Ensemble": "user_majority_predictions.csv",
        "vM LR": "vmlr_predictions.csv",
    }
    y = pd.read_csv(DATA_PATH / "Y_values_AISnan_1812.csv", index_col=0)["SCIM2_12"]
    fpr_tpr_auc_dict_all = {}
    fpr_tpr_auc_dict_all_by_ais = {}

    for model, file in model_predictions.items():
        predictions = pd.read_csv(
            DATA_PATH / "results" / "full-cohort" / file, index_col=0
        )
        fpr, tpr, _ = roc_curve(y, predictions["proba"])
        auc = roc_auc_score(y, predictions["proba"])
        fpr_tpr_auc_dict_model = {model: {"fpr": fpr, "tpr": tpr, "auc": auc}}
        fpr_tpr_auc_dict_all[model] = {"fpr": fpr, "tpr": tpr, "auc": auc}
        fig, ax = plt.subplots(1, 1, figsize=(5, 5))
        _ = plot_roc_curve(ax, fpr_tpr_auc_dict_model, add_title=True, add_legend=False)
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / (file.split(".")[0] + "_roc.pdf"),
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()

        fpr_tpr_auc_dict_by_ais = {}
        for ais in predictions["AIS"].unique():
            ais_sub = predictions[predictions["AIS"] == ais].index
            fpr, tpr, _ = roc_curve(y.loc[ais_sub], predictions.loc[ais_sub, "proba"])
            auc = roc_auc_score(y.loc[ais_sub], predictions.loc[ais_sub, "proba"])
            fpr_tpr_auc_dict_by_ais[ais] = {"fpr": fpr, "tpr": tpr, "auc": auc}
        fpr_tpr_auc_dict_all_by_ais[model] = fpr_tpr_auc_dict_by_ais
        fig, ax = plt.subplots(1, 1, figsize=(5.5, 5.5))
        _ = plot_roc_curve(
            ax, fpr_tpr_auc_dict_by_ais, add_title=False, add_legend=True
        )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / (file.split(".")[0] + "_roc_by_ais.pdf"),
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()

    fig, ax = plt.subplots(1, 1, figsize=(5.5, 5.5))
    _ = plot_roc_curve(
        ax, fpr_tpr_auc_dict_all, add_title=False, add_legend=True, add_random=True
    )
    fig.tight_layout()
    fig.savefig(
        PLOTS_PATH / "all_models_roc.pdf",
        transparent=True,
        dpi=200,
        format="pdf",
    )
    plt.close()

    # plot by AIS grade
    for ais in predictions["AIS"].unique():
        fpr_tpr_auc_dict_by_ais = {
            m: fpr_tpr_auc_dict_all_by_ais[m][ais]
            for m in fpr_tpr_auc_dict_all_by_ais.keys()
        }
        fig, ax = plt.subplots(1, 1, figsize=(5.5, 5.5))
        _ = plot_roc_curve(
            ax,
            fpr_tpr_auc_dict_by_ais,
            add_title=False,
            add_legend=True,
            add_random=True,
        )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / f"all_models_roc_AIS_{ais}.pdf",
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()

    # plot for models retrained for specific AIS grade
    files_retrained = {
        "B": {
            "all-feature LR": "lr_ais_b_test_set_predictions.csv",
            "XGBoost": "xgboost_ais_b_test_set_predictions.csv",
        },
        "C": {
            "all-feature LR": "lr_ais_c_test_set_predictions.csv",
            "XGBoost": "xgboost_ais_c_test_set_predictions.csv",
        },
    }
    for ais in ["B", "C"]:
        fpr_tpr_auc_dict_by_ais = {
            "Physician Ensemble": fpr_tpr_auc_dict_all_by_ais["Physician Ensemble"][ais]
        }
        for model, file in files_retrained[ais].items():
            predictions = pd.read_csv(
                DATA_PATH / "results" / "retrain-aisb-aisc" / file, index_col=0
            )
            ais_sub = predictions.index
            fpr, tpr, _ = roc_curve(y.loc[ais_sub], predictions["proba"])
            auc = roc_auc_score(y.loc[ais_sub], predictions["proba"])
            fpr_tpr_auc_dict_by_ais[model] = {"fpr": fpr, "tpr": tpr, "auc": auc}

        fig, ax = plt.subplots(1, 1, figsize=(5.5, 5.5))
        _ = plot_roc_curve(
            ax,
            fpr_tpr_auc_dict_by_ais,
            add_title=False,
            add_legend=True,
            add_random=True,
        )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / f"all_models_roc_AIS_{ais}_retrained.pdf",
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()
