"""Plot Venn diagrams to illustrate shared/distinct patients wrongly predicted by
different models.

Authors: Julia Bugajska, Louis Lukas
"""

from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib_venn import venn3


DATA_PATH = Path("")
PLOTS_PATH = Path("")
COLOURS = {
    # https://www.color-hex.com/color-palette/1047605
    # https://www.color-hex.com/color-palette/1047606
    "all-feature LR": "#cf0072",
    "XGBoost": "#4f2d7f",
    "Physician Ensemble": "#3f9c35",
    "Unanimous Agreement": "#fed100",
    "vM LR": "#0094b3",
}
PLOT_MARGIN = 0.05


def plot_venn3_annotated(  # pylint: disable=R0914
    ax: plt.axis,  # pylint: disable=W0621
    incorrect_predictions: dict,
    ais_grades: pd.Series = None,  # pylint: disable=W0621
    title: str = None,
):
    """Plot Venn diagram for incorrect predictions of three models."""
    fontsize_title = 20
    fontsize_label = 20
    fontsize_annot = 14

    offsets = {
        "Physician Ensemble": {
            "100": (-100, -2),
            "010": (100, -11),
            "001": (-100, -20),
            "110": (0, 80),
            "011": (100, -50),
            "101": (-110, 0),
        },
        "Unanimous Agreement": {
            "100": (-80, 10),
            "010": (120, -10),
            "001": (-80, -10),
            "110": (20, 100),
            "011": (100, -50),
            "101": (-90, 0),
        },
    }

    models = sorted(incorrect_predictions.keys(), key=str.lower)
    assert len(models) == 3
    assert models[0] == "all-feature LR"
    assert models[2] == "XGBoost"

    venn = venn3(  # pylint: disable=W0621
        subsets=[incorrect_predictions[model] for model in models],
        set_labels=models,
        set_colors=[COLOURS[model] for model in models],
        alpha=0.25,  # default: 0.4
    )
    for subset_id in list(offsets["Physician Ensemble"].keys()) + ["111"]:
        try:
            venn.get_label_by_id(subset_id).set_text(
                r"\textbf{" + venn.get_label_by_id(subset_id).get_text() + r"}"
            )
            venn.get_label_by_id(subset_id).set_fontsize(fontsize_label)
        except AttributeError:  # some subset labels might not exist because there
            pass  # is no wrongly predicted patient in a particular intersection

    for set_id in ["A", "B", "C"]:
        venn.get_label_by_id(set_id).set_text(
            r"\textbf{" + venn.get_label_by_id(set_id).get_text() + r"}"
        )
        venn.get_label_by_id(set_id).set_fontsize(fontsize_label)

    if ais_grades is not None:
        subsets = {
            "100": set(incorrect_predictions[models[0]]).difference(  # = all-feature LR
                set(incorrect_predictions[models[1]]).union(
                    incorrect_predictions[models[2]]
                )
            ),
            "010": set(  # = physician model
                incorrect_predictions[models[1]]
            ).difference(
                set(incorrect_predictions[models[0]]).union(
                    incorrect_predictions[models[2]]
                )
            ),
            "001": set(incorrect_predictions[models[2]]).difference(  # = XGBoost
                set(incorrect_predictions[models[0]]).union(
                    incorrect_predictions[models[1]]
                )
            ),
            "110": (  # = all-feature intersect physician model
                set(incorrect_predictions[models[0]]).intersection(
                    set(incorrect_predictions[models[1]])
                )
            ).difference(set(incorrect_predictions[models[2]])),
            "011": (  # = physician model intersect XGBoost
                set(incorrect_predictions[models[1]]).intersection(
                    set(incorrect_predictions[models[2]])
                )
            ).difference(set(incorrect_predictions[models[0]])),
            "101": (  # = all-feature LR intersect XGBoost
                set(incorrect_predictions[models[0]]).intersection(
                    set(incorrect_predictions[models[2]])
                )
            ).difference(set(incorrect_predictions[models[1]])),
            "111": set(incorrect_predictions[models[0]])  # = intersect all
            & set(incorrect_predictions[models[1]])
            & set(incorrect_predictions[models[2]]),
        }
        for subset_id, subset_patients in subsets.items():
            ais_grade_dist = ais_grades[
                list(subset_patients)
            ].value_counts().sort_index() / len(subset_patients)
            ais_grade_dist = (ais_grade_dist * 100).round(decimals=1)
            # build table as text
            annotation_str = r"\begin{tabular}{lr}"
            annotation_str += r"$\mathbf{n=" + f"{len(subset_patients)}" + r"}$&\\"
            for i, ais_grade in enumerate(ais_grade_dist.index):
                annotation_str += f"AIS {ais_grade}" + r"&"
                annotation_str += r"$" + f"{ais_grade_dist[ais_grade]}" + r"\%$"
                if i < ais_grade_dist.shape[0] - 1:
                    annotation_str += r"\\"
            annotation_str += r"\end{tabular}"
            # place annotation on venn diagram
            if subset_id == "111":
                venn.get_label_by_id(subset_id).set_text(annotation_str)
                venn.get_label_by_id(subset_id).set_size(fontsize_annot)
            else:
                venn.get_label_by_id(subset_id).set_text("")  # remove text inside patch
                patch_center = venn.get_label_by_id(subset_id).get_position()
                ax.annotate(
                    annotation_str,
                    xy=patch_center,
                    xytext=offsets[models[1]][subset_id],
                    textcoords="offset points",
                    fontsize=fontsize_annot,
                    bbox=dict(boxstyle="round,pad=0.15", fc="gray", alpha=0.1),
                    arrowprops=dict(
                        arrowstyle="->",
                        connectionstyle="arc3,rad=0.3",
                        color="gray",
                        alpha=0.6,
                    ),  # Adjust rad for curvature
                    ha="center",
                )
            # vertical / horizontal alignment will depend on location
            venn.get_label_by_id(subset_id).set_va("center")
            venn.get_label_by_id(subset_id).set_ha("center")

    if title:
        ax.set_title(
            r"\textbf{" + title + r"}",
            fontsize=fontsize_title,
        )

    return ax, venn


if __name__ == "__main__":
    plt.rcParams.update({"font.family": "serif", "font.serif": ["Times New Roman"]})
    plt.rc("text", usetex=True)
    model_predictions = {
        "all-feature LR": "lr_test_set_predictions.csv",
        "XGBoost": "xgboost_test_set_predictions.csv",
        "Physician Ensemble": "user_majority_predictions.csv",
        "Unanimous Agreement": "user_unanimous_predictions.csv",
        "vM LR": "vmlr_predictions.csv",
    }
    model_incorrect_predictions = {}
    for model, file in model_predictions.items():
        predictions = pd.read_csv(
            DATA_PATH / "results" / "full-cohort" / file, index_col=0
        )
        model_incorrect_predictions[model] = set(
            predictions[~predictions["correct"]].index.to_list()
        )
    ais_grades = pd.read_csv(
        DATA_PATH / "X_values_non_summarized_AISnan_1812.csv", index_col=0
    )["AIS"]

    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    _, venn = plot_venn3_annotated(
        ax,
        {
            k: model_incorrect_predictions[k]
            for k in ["all-feature LR", "Physician Ensemble", "XGBoost"]
        },
        ais_grades=ais_grades,
    )
    # x0, x1, y0, y1 = plt.axis()
    # plt.axis((x0 - PLOT_MARGIN, x1 + PLOT_MARGIN, y0 - PLOT_MARGIN, y1 + PLOT_MARGIN))
    fig.tight_layout()
    fig.savefig(
        PLOTS_PATH / "venn_allfeature_xgb_ensemble.pdf",
        transparent=True,
        dpi=200,
        format="pdf",
    )
    plt.close()

    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    _, venn = plot_venn3_annotated(
        ax,
        {
            k: model_incorrect_predictions[k]
            for k in ["all-feature LR", "Unanimous Agreement", "XGBoost"]
        },
        ais_grades=ais_grades,
    )
    fig.tight_layout()
    x0, x1, y0, y1 = plt.axis()
    plt.axis((x0 - 0.25, x1 + 0.25, y0, y1))
    fig.savefig(
        PLOTS_PATH / "venn_allfeature_xgb_unanimous.pdf",
        transparent=True,
        dpi=200,
        format="pdf",
    )
    plt.close()

    # plot by AIS grade
    for ais in sorted(ais_grades.unique()):
        ais_subset = ais_grades[ais_grades == ais].index
        model_incorrect_predictions_ais = {}
        for model, file in model_predictions.items():
            predictions = pd.read_csv(
                DATA_PATH / "results" / "full-cohort" / file, index_col=0
            )
            predictions_ais = predictions[predictions["AIS"] == ais]
            model_incorrect_predictions_ais[model] = set(
                predictions_ais[~predictions_ais["correct"]].index.to_list()
            )
        fig, ax = plt.subplots(1, 1, figsize=(8, 6))
        _, venn = plot_venn3_annotated(
            ax,
            {
                k: model_incorrect_predictions_ais[k]
                for k in ["all-feature LR", "Physician Ensemble", "XGBoost"]
            },
            title=f"AIS {ais}",
        )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / f"venn_allfeature_xgb_ensemble_AIS-{ais}.pdf",
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()

        fig, ax = plt.subplots(1, 1, figsize=(8, 6))
        _, venn = plot_venn3_annotated(
            ax,
            {
                k: model_incorrect_predictions_ais[k]
                for k in ["all-feature LR", "Unanimous Agreement", "XGBoost"]
            },
            title=f"AIS {ais}",
        )
        fig.tight_layout()
        fig.tight_layout()
        x0, x1, y0, y1 = plt.axis()
        plt.axis((x0 - 0.25, x1 + 0.25, y0, y1))
        fig.savefig(
            PLOTS_PATH / f"venn_allfeature_xgb_unanimous_AIS-{ais}.pdf",
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()

    # plot for models retrained for specific AIS grade
    files_retrained = {
        "B": {
            "all-feature LR": "lr_ais_b_test_set_predictions.csv",
            "XGBoost": "xgboost_ais_b_test_set_predictions.csv",
            "Physician Ensemble": "user_majority_predictions.csv",
            "Unanimous Agreement": "user_unanimous_predictions.csv",
        },
        "C": {
            "all-feature LR": "lr_ais_c_test_set_predictions.csv",
            "XGBoost": "xgboost_ais_c_test_set_predictions.csv",
            "Physician Ensemble": "user_majority_predictions.csv",
            "Unanimous Agreement": "user_unanimous_predictions.csv",
        },
    }
    for ais in ["B", "C"]:
        model_incorrect_predictions_ais = {}
        for model, file in files_retrained[ais].items():
            if model in ["all-feature LR", "XGBoost"]:
                predictions = pd.read_csv(
                    DATA_PATH / "results" / "retrain-aisb-aisc" / file, index_col=0
                )
            else:
                predictions = pd.read_csv(
                    DATA_PATH / "results" / "full-cohort" / file, index_col=0
                )
            predictions_ais = predictions[predictions["AIS"] == ais]
            model_incorrect_predictions_ais[model] = set(
                predictions_ais[~predictions_ais["correct"]].index.to_list()
            )
        fig, ax = plt.subplots(1, 1, figsize=(8, 6))
        _, venn = plot_venn3_annotated(
            ax,
            {
                k: model_incorrect_predictions_ais[k]
                for k in ["all-feature LR", "Physician Ensemble", "XGBoost"]
            },
            title=f"AIS {ais}",
        )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / f"venn_allfeature_xgb_ensemble_AIS-{ais}_retrained.pdf",
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()

        fig, ax = plt.subplots(1, 1, figsize=(8, 6))
        _, venn = plot_venn3_annotated(
            ax,
            {
                k: model_incorrect_predictions_ais[k]
                for k in ["all-feature LR", "Unanimous Agreement", "XGBoost"]
            },
            title=f"AIS {ais}",
        )
        fig.tight_layout()
        fig.tight_layout()
        x0, x1, y0, y1 = plt.axis()
        plt.axis((x0 - 0.25, x1 + 0.25, y0, y1))
        fig.savefig(
            PLOTS_PATH / f"venn_allfeature_xgb_unanimous_AIS-{ais}_retrained.pdf",
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()
