"""Hyperparameter tuning and training for all feature logistic regression (lr).

@author: Julia Bugajska, Louis Lukas
"""

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import shap
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    precision_score,
    recall_score,
    roc_auc_score,
)
from sklearn.model_selection import ParameterGrid, StratifiedKFold

import sci_walking_app.definitions as definitions

DATA_PATH = Path("")
RESULTS_PATH = Path("")

RANDOM_SEED = 10
VERBOSE = False


if __name__ == "__main__":
    if not RESULTS_PATH.exists():
        RESULTS_PATH.mkdir(parents=True, exist_ok=True)
    X_raw = pd.read_csv(
        DATA_PATH / "X_values_non_summarized_AISnan_1812.csv",
        index_col=0,
    )
    y_raw = pd.read_csv(DATA_PATH / "Y_values_AISnan_1812.csv", index_col=0)

    # know number of patients to be included; make sure this number is correct
    assert X_raw.shape[0] == 794
    assert y_raw.shape[0] == 794

    # scale motor and sensory columns
    X_raw[definitions.SENSORY_COLUMNS] = X_raw[definitions.SENSORY_COLUMNS] / 2
    X_raw[definitions.MOTOR_COLUMNS] = X_raw[definitions.MOTOR_COLUMNS] / 5
    # ...and drop columns for exam stage and exact age
    X_raw.drop(["ExamStage_weeks", "AgeAtDOI"], axis="columns", inplace=True)

    # create "joint" label for stratification of train-test/k-fold split
    stratify_by = X_raw["AIS"] + "_" + y_raw["SCIM2_12"].astype(str)
    print("Distribution of outcome stratified by AIS grade:")
    stratify_by_dist = stratify_by.value_counts().sort_index() / stratify_by.shape[0]
    print(stratify_by_dist.round(3))
    # remove AIS grade from features
    X = X_raw.drop("AIS", axis=1)
    # ...and use only outcome
    y = y_raw["SCIM2_12"]

    # store test set predictions to calculate metrics etc
    y_test = pd.DataFrame(index=y.index.copy(deep=True), columns=["proba", "label"])
    # nested CV -- stratified by both AIS grade and label
    outer_split = StratifiedKFold(
        n_splits=5,
        shuffle=True,
        random_state=RANDOM_SEED,
    )
    param_grid = {
        "C": [1e-3, 1e-2, 1e-1, 1, 1e1, 1e2, 1e3],
        "l1_ratio": np.arange(0.0, 1.1, 0.1),
    }
    param_list = list(ParameterGrid(param_grid))
    best_parameters = pd.DataFrame(index=range(1, 6), columns=param_grid.keys())
    shap_values = pd.DataFrame(
        index=X.index.copy(deep=True),
        columns=X.columns.copy(deep=True),
    )

    for outer_fold, (train_outer, test_outer) in enumerate(
        outer_split.split(X, stratify_by)
    ):
        print(f"outer fold {outer_fold + 1} / 5")
        X_train_outer, y_train_outer = X.iloc[train_outer, :], y.iloc[train_outer]
        X_test_outer, y_test_outer = X.iloc[test_outer, :], y.iloc[test_outer]
        if VERBOSE:
            print(f" train size outer: {X_train_outer.shape[0]}")
            print(f" test size outer: {X_test_outer.shape[0]}")
        inner_split = StratifiedKFold(
            n_splits=5,
            shuffle=True,
            random_state=RANDOM_SEED + outer_fold,
        )
        roc_aucs = pd.DataFrame(
            index=range(0, len(param_list)),
            columns=range(1, 6),
        )
        assert np.allclose(
            stratify_by_dist,
            (
                stratify_by.iloc[train_outer].value_counts().sort_index()
                / stratify_by.iloc[train_outer].shape[0]
            ),
            rtol=1e-3,
            atol=2.5 * 1e-2,  # allow absolute deviation by 2.5%
        )
        assert np.allclose(
            stratify_by_dist,
            (
                stratify_by.iloc[test_outer].value_counts().sort_index()
                / stratify_by.iloc[test_outer].shape[0]
            ),
            rtol=1e-3,
            atol=2.5 * 1e-2,  # allow absolute deviation by 2.5%
        )
        for param_id, parameters in enumerate(param_list):
            for inner_fold, (train_inner, test_inner) in enumerate(
                inner_split.split(X_train_outer, stratify_by.iloc[train_outer])
            ):
                if VERBOSE:
                    print(f"\tinner fold {inner_fold + 1 } / 5")
                assert np.allclose(
                    stratify_by_dist,
                    (
                        stratify_by.iloc[train_inner].value_counts().sort_index()
                        / stratify_by.iloc[train_inner].shape[0]
                    ),
                    rtol=1e-2,
                    atol=5 * 1e-2,  # allow absolute deviation by 5%
                )
                try:
                    assert np.allclose(
                        stratify_by_dist,
                        (
                            stratify_by.iloc[test_inner].value_counts().sort_index()
                            / stratify_by.iloc[test_inner].shape[0]
                        ),
                        rtol=1e-2,
                        atol=1e-1,  # allow absolute deviation by 10%
                    )
                except ValueError:
                    if VERBOSE:
                        print(
                            "Labels in inner test set:"
                            + f" {stratify_by.iloc[test_inner].value_counts().shape[0]}"
                        )
                X_train_inner = X_train_outer.iloc[train_inner, :]
                y_train_inner = y_train_outer.iloc[train_inner]
                X_test_inner = X_train_outer.iloc[test_inner, :]
                y_test_inner = y_train_outer.iloc[test_inner]

                lr_clf = LogisticRegression(
                    C=parameters["C"],
                    l1_ratio=parameters["l1_ratio"],
                    penalty="elasticnet",
                    solver="saga",
                    max_iter=10000,
                    random_state=RANDOM_SEED,
                )
                lr_clf.fit(X_train_inner, y_train_inner)
                y_pred = lr_clf.predict_proba(X_test_inner)[:, 1]
                roc_aucs.loc[param_id + 1, inner_fold + 1] = roc_auc_score(
                    y_true=y_test_inner, y_score=y_pred
                )

        best_hyperpars_idx = roc_aucs.mean(axis=1).idxmax()
        best_hyperpars = param_list[best_hyperpars_idx]
        for par in best_hyperpars.keys():
            best_parameters.loc[outer_fold + 1, par] = best_hyperpars[par]
        lr_clf_best = LogisticRegression(
            C=best_hyperpars["C"],
            l1_ratio=best_hyperpars["l1_ratio"],
            penalty="elasticnet",
            solver="saga",
            max_iter=10000,
            random_state=RANDOM_SEED,
        )

        lr_clf_best.fit(X_train_outer, y_train_outer)
        y_test.loc[X_test_outer.index, "proba"] = lr_clf_best.predict_proba(
            X_test_outer
        )[:, 1]
        explainer = shap.Explainer(lr_clf_best, X_train_outer)
        shap_values.loc[X_test_outer.index, :] = explainer.shap_values(X_test_outer)

    y_test["label"] = y_test["proba"].apply(lambda x: 1 if x >= 0.5 else 0)
    y_test["correct"] = y_test["label"] == y
    y_test = y_test.join(X_raw["AIS"])
    performance = pd.DataFrame(
        index=["overall", "AIS_A", "AIS_B", "AIS_C", "AIS_D"],
        columns=[
            "accuracy",
            "precision",
            "recall",
            "specificity",
            "roc_auc",
        ],
    )
    test_accuracy = accuracy_score(y, y_test["label"])
    test_precision = precision_score(y, y_test["label"])
    test_recall = recall_score(y, y_test["label"])
    test_roc_auc = roc_auc_score(y, y_test["proba"])
    tn, fp, fn, tp = confusion_matrix(y, y_test["label"]).ravel()
    test_specificity = tn / (tn + fp)
    print("Performance metrics (test set)")
    print(f"\taccuracy={test_accuracy:0.4f}")
    print(f"\tprecision={test_precision:0.4f}")
    print(f"\trecall={test_recall:0.4f}")
    print(f"\tspecificity={test_specificity:0.4f}")
    print(f"\tROC-AUC={test_roc_auc:0.4f}")
    performance.loc["overall", "accuracy"] = test_accuracy
    performance.loc["overall", "precision"] = test_precision
    performance.loc["overall", "recall"] = test_recall
    performance.loc["overall", "specificity"] = test_specificity
    performance.loc["overall", "roc_auc"] = test_roc_auc
    for ais in y_test["AIS"].unique():
        if VERBOSE:
            print(f"\tAIS {ais}")
        y_test_sub = y_test[y_test["AIS"] == ais]
        y_sub = y[y_test_sub.index]
        test_accuracy_by_ais = accuracy_score(y_sub, y_test_sub["label"])
        test_precision_by_ais = precision_score(y_sub, y_test_sub["label"])
        test_recall_by_ais = recall_score(y_sub, y_test_sub["label"])
        test_roc_auc_by_ais = roc_auc_score(y_sub, y_test_sub["proba"])
        tn, fp, fn, tp = confusion_matrix(y_sub, y_test_sub["label"]).ravel()
        test_specificity_by_ais = tn / (tn + fp)
        performance.loc[f"AIS_{ais}", "accuracy"] = test_accuracy_by_ais
        performance.loc[f"AIS_{ais}", "precision"] = test_precision_by_ais
        performance.loc[f"AIS_{ais}", "recall"] = test_recall_by_ais
        performance.loc[f"AIS_{ais}", "specificity"] = test_specificity_by_ais
        performance.loc[f"AIS_{ais}", "roc_auc"] = test_roc_auc_by_ais
        if VERBOSE:
            print(f"\t\taccuracy={test_accuracy_by_ais:0.4f}")
            print(f"\t\tprecision={test_precision_by_ais:0.4f}")
            print(f"\t\trecall={test_recall_by_ais:0.4f}")
            print(f"\t\tspecificity={test_specificity_by_ais:0.4f}")
            print(f"\t\tROC-AUC={test_roc_auc_by_ais:0.4f}")

    y_test.to_csv(RESULTS_PATH / "lr_test_set_predictions.csv")
    performance.to_csv(RESULTS_PATH / "lr_test_set_performance.csv")
    shap.summary_plot(
        shap_values.astype("float32").values,
        features=X,
        feature_names=shap_values.columns,
        max_display=10,
        show=False,
    )
    plt.savefig(
        RESULTS_PATH / "lr_test_set_shapsummary.pdf",
        format="pdf",
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()
    shap.summary_plot(
        shap_values.astype("float32").values,
        features=X,
        feature_names=shap_values.columns,
        max_display=10,
        show=False,
        plot_size=[4, 8],
    )
    ax = plt.gca()
    ax.set_xlabel("SHAP Value")
    plt.savefig(
        RESULTS_PATH / "lr_test_set_shapsummary_narrow.pdf",
        format="pdf",
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()
    shap_values.to_csv(RESULTS_PATH / "lr_test_set_shapvalues.csv")
