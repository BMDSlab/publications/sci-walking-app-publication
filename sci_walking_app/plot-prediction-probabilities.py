"""Plot prediction probabilities for all models.

Authors: Julia Bugajska, Louis Lukas
"""

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

DATA_PATH = Path("")
PLOTS_PATH = Path("")


def individual_probability_plot(
    ax: plt.axis,  # pylint: disable=W0621
    predictions: pd.DataFrame,  # pylint: disable=W0621
    model_name: str,
    n_bins: int = 20,
    y_max: int = 12,
    add_legend: bool = False,
):
    """
    ax : matplotlib.pyplot.axis
        Axis instance on which to plot histogram of probabilities.
    predictions: pd.DataFrame
        DataFrame with at least two columns: predicted probabilities ("proba"),
        indicator whether prediction was correct ("correct").
    model_name : str
        Name of model from which probabilities were derived; used for title.
    n_bins : int
        Number of bins to use in histogram of probabilities.
    y_max : int
        Upper limit of y-axis.
    add_legend : bool
        Flag indicating whether legend should be added.
    """
    alpha = 0.4
    color_correct = "blue"
    color_incorrect = "red"
    fontsize_label = 20
    fontsize_legend = 16
    fontsize_ticks = 16
    fontsize_title = 20

    ax.hist(
        predictions[predictions["correct"]]["proba"],
        bins=[i / n_bins for i in range(0, n_bins + 1, 1)],
        color=color_correct,
        alpha=alpha,
        label="correct predictions",
        density=True,
    )
    ax.hist(
        predictions[~predictions["correct"]]["proba"],
        bins=[i / n_bins for i in range(0, n_bins + 1, 1)],
        color=color_incorrect,
        alpha=alpha,
        label="incorrect predictions",
        density=True,
    )
    ax.set_title(
        model_name,  # "prediction probabilities for " +
        fontsize=fontsize_title,
        fontweight="bold",
    )
    ax.set_xlim(0, 1)
    ax.set_ylim(0, y_max)
    ax.set_xticks(
        np.arange(0, 1.1, 0.25),
        [f"{i}" for i in np.arange(0, 1.1, 0.25)],
        fontsize=fontsize_ticks,
    )
    ax.set_yticks(
        np.arange(0, y_max + 1, 2),
        [f"{i}" for i in np.arange(0, y_max + 1, 2)],
        fontsize=fontsize_ticks,
    )
    ax.set_xlabel("prediction probability", fontsize=fontsize_label)
    ax.set_ylabel("frequency", fontsize=fontsize_label)
    if add_legend:
        ax.legend(fontsize=fontsize_legend)

    return ax


if __name__ == "__main__":
    plt.rcParams.update({"font.family": "serif", "font.serif": ["Times New Roman"]})
    model_predictions = {
        "all-feature LR": "lr_test_set_predictions.csv",
        "XGBoost": "xgboost_test_set_predictions.csv",
        "Physician Ensemble": "user_majority_predictions.csv",
        "vM LR": "vmlr_predictions.csv",
    }

    for model, file in model_predictions.items():
        predictions = pd.read_csv(
            DATA_PATH / "results" / "full-cohort" / file, index_col=0
        )
        fig, ax = plt.subplots(1, 1, figsize=(5, 5))
        _ = individual_probability_plot(
            ax=ax, predictions=predictions, model_name=model, add_legend=True
        )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / (file.split(".")[0] + "_probabilities.pdf"),
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()
        fig, axs = plt.subplots(nrows=1, ncols=4, figsize=(20, 5))
        for i, ais in enumerate(sorted(predictions["AIS"].unique().tolist())):
            _ = individual_probability_plot(
                ax=axs[i],
                predictions=predictions[predictions["AIS"] == ais],
                model_name=model + " (AIS " + ais + ")",
                y_max=20,
                add_legend=True if i == 0 else False,
            )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / (file.split(".")[0] + "_probabilities_by_AIS.pdf"),
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()

        for i, ais in enumerate(sorted(predictions["AIS"].unique().tolist())):
            fig, ax = plt.subplots(1, 1, figsize=(5, 5))
            _ = individual_probability_plot(
                ax=ax,
                predictions=predictions[predictions["AIS"] == ais],
                model_name=model + " (AIS " + ais + ")",
                add_legend=True,
            )
            fig.tight_layout()
            fig.savefig(
                PLOTS_PATH / (file.split(".")[0] + f"_probabilities_AIS_{ais}.pdf"),
                transparent=True,
                dpi=200,
                format="pdf",
            )
            plt.close()
