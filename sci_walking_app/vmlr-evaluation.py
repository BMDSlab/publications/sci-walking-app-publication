"""Implement prediction rule from van Middendorp et al. (2011)
[doi.org/10.1016/S0140-6736(10)62276-3].

Predictors:
    - age, binarised at 65
    - motor score L3, max of left/right
    - motor score S1, max of left/right
    - light touch L3, max of left/right
    - light touch S1, max of left/right

Outcome:
    walking outcome according to SCIM item 12 (indoor mobility) biarised
    at 3; score <= 3 defines dependent walkers (don't walk), score >= 4
    defines independent walkers (walk)

Author(s): Julia Bugajska, Louis Lukas
"""

from math import exp
from pathlib import Path

import pandas as pd
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    precision_score,
    recall_score,
    roc_auc_score,
)

DATA_PATH = Path("")
RESULTS_PATH = Path("")
VERBOSE = False


def convert_data(data: pd.DataFrame):
    """Convert full set of neurological features into features as used by van Middendorp
    prediction rule.

    - maximum score value from left and right
    - binarise age
    """
    X_vm = pd.DataFrame(columns=["age_bin", "MS_L3", "MS_S1", "LT_L3", "LT_S1"])
    X_vm["age_bin"] = data["AgeAtDOI_binarised"]
    X_vm["MS_L3"] = data[["RMS_L3", "LMS_L3"]].max(axis=1)
    X_vm["MS_S1"] = data[["RMS_S1", "LMS_S1"]].max(axis=1)
    X_vm["LT_L3"] = data[["RLT_L3", "LLT_L3"]].max(axis=1)
    X_vm["LT_S1"] = data[["RLT_S1", "LLT_S1"]].max(axis=1)

    return X_vm


def predict_vmdrp(
    X: pd.DataFrame, threshold: float = 0.5  # pylint: disable=C0103,W0621
) -> pd.Series:
    """Predict walking outcome as defined via SCIM subitem 12 (see above).

    Args
    ----
    X: pd.DataFrame

    threshold: float


    Returns
    -------
    """
    pred_score = (
        -10 * X["age_bin"]
        + 2 * X["MS_L3"]
        + 2 * X["MS_S1"]
        + 5 * X["LT_L3"]
        + 5 * X["LT_S1"]
    )
    assert pred_score.max() <= 40
    assert pred_score.min() >= -10
    # equation as reported on bottom of page 1007
    proba = pred_score.apply(
        lambda x: exp(-3.273 + 0.267 * x) / (1 + exp(-3.279 + 0.267 * x))
    )
    proba.name = "proba"
    # some observed values slightly larger than 1, likely due to rounding
    #   of coefficients reported in paper
    proba.clip(lower=0, upper=1, inplace=True)

    pred = proba > threshold
    pred.name = "label"

    return proba, pred


if __name__ == "__main__":
    X_raw = pd.read_csv(
        DATA_PATH / "X_values_non_summarized_AISnan_1812.csv",
        index_col=0,
    )
    y_raw = pd.read_csv(DATA_PATH / "Y_values_AISnan_1812.csv", index_col=0)
    X = convert_data(X_raw)
    y = y_raw["SCIM2_12"]
    pred_proba, pred_label = predict_vmdrp(X)
    out = pd.DataFrame(index=y.index.copy(deep=True))
    out = out.join(pred_proba)
    out = out.join(pred_label)
    out["correct"] = out["label"] == y
    out = out.join(X_raw["AIS"])
    performance = pd.DataFrame(
        index=["overall", "AIS_A", "AIS_B", "AIS_C", "AIS_D"],
        columns=[
            "accuracy",
            "precision",
            "recall",
            "specificity",
            "roc_auc",
        ],
    )
    test_accuracy = accuracy_score(y, out["label"])
    test_precision = precision_score(y, out["label"])
    test_recall = recall_score(y, out["label"])
    test_roc_auc = roc_auc_score(y, out["proba"])
    tn, fp, fn, tp = confusion_matrix(y, out["label"]).ravel()
    test_specificity = tn / (tn + fp)
    print("Performance metrics (test set)")
    print(f"\taccuracy={test_accuracy:0.4f}")
    print(f"\tprecision={test_precision:0.4f}")
    print(f"\trecall={test_recall:0.4f}")
    print(f"\tspecificity={test_specificity:0.4f}")
    print(f"\tROC-AUC={test_roc_auc:0.4f}")
    performance.loc["overall", "accuracy"] = test_accuracy
    performance.loc["overall", "precision"] = test_precision
    performance.loc["overall", "recall"] = test_recall
    performance.loc["overall", "specificity"] = test_specificity
    performance.loc["overall", "roc_auc"] = test_roc_auc
    for ais in out["AIS"].unique():
        if VERBOSE:
            print(f"\tAIS {ais}")
        out_sub = out[out["AIS"] == ais]
        y_sub = y[out_sub.index]
        test_accuracy_by_ais = accuracy_score(y_sub, out_sub["label"])
        test_precision_by_ais = precision_score(y_sub, out_sub["label"])
        test_recall_by_ais = recall_score(y_sub, out_sub["label"])
        test_roc_auc_by_ais = roc_auc_score(y_sub, out_sub["proba"])
        tn, fp, fn, tp = confusion_matrix(y_sub, out_sub["label"]).ravel()
        test_specificity_by_ais = tn / (tn + fp)
        performance.loc[f"AIS_{ais}", "accuracy"] = test_accuracy_by_ais
        performance.loc[f"AIS_{ais}", "precision"] = test_precision_by_ais
        performance.loc[f"AIS_{ais}", "recall"] = test_recall_by_ais
        performance.loc[f"AIS_{ais}", "specificity"] = test_specificity_by_ais
        performance.loc[f"AIS_{ais}", "roc_auc"] = test_roc_auc_by_ais
        if VERBOSE:
            print(f"\t\taccuracy={test_accuracy_by_ais:0.4f}")
            print(f"\t\tprecision={test_precision_by_ais:0.4f}")
            print(f"\t\trecall={test_recall_by_ais:0.4f}")
            print(f"\t\tspecificity={test_specificity_by_ais:0.4f}")
            print(f"\t\tROC-AUC={test_roc_auc_by_ais:0.4f}")

    out.to_csv(RESULTS_PATH / "vmlr_predictions.csv")
    performance.to_csv(RESULTS_PATH / "vmlr_performance.csv")
