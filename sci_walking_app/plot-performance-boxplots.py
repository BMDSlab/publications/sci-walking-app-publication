"""Generate boxplots showing performance of individual doctors in comparison with vm LR.

Authors: Julia Bugajska, Louis Lukas
"""

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.metrics import accuracy_score, confusion_matrix, recall_score

DATA_PATH = Path("")
PLOTS_PATH = Path("")
COLOURS = {  # ...for AIS grades
    # https://www.color-hex.com/color-palette/1047605
    # https://www.color-hex.com/color-palette/1047606
    "A": "#cf0072",
    "B": "#4f2d7f",
    "C": "#3f9c35",
    "D": "#0094b3",
}
COLOUR_USERS = "#0094b3"


def _compute_performance_by_user(
    user_predictions: pd.DataFrame,  # pylint: disable=W0621
):
    """Compute accuracy, recall, and specificity from dataframe with individual
    predictions in long format.
    """
    performance_by_user = pd.DataFrame(  # pylint: disable=W0621
        index=user_predictions["user_id"].unique(),
        columns=["accuracy", "recall", "specificity"],
    ).sort_index()
    for user in performance_by_user.index:
        predictions_by_user = user_predictions[user_predictions["user_id"] == user]
        performance_by_user.loc[user, "accuracy"] = accuracy_score(
            predictions_by_user["correctanswer"], predictions_by_user["useranswer"]
        )
        performance_by_user.loc[user, "recall"] = recall_score(
            predictions_by_user["correctanswer"], predictions_by_user["useranswer"]
        )
        tn, fp, fn, tp = confusion_matrix(  # pylint: disable=W0612
            predictions_by_user["correctanswer"], predictions_by_user["useranswer"]
        ).ravel()
        performance_by_user.loc[user, "specificity"] = tn / (tn + fp)

    return performance_by_user


def individual_boxplot(
    ax: plt.axis,  # pylint: disable=W0621
    users_performance: pd.DataFrame,
    vmlr_performance: pd.DataFrame,
    colour_users: str = COLOUR_USERS,
    label_users: str = "Physicians",
    title: str = None,
    add_legend: bool = False,
    lower_y: float = 0.5,
):
    """Plot boxplots of user performance and vm LR for comparison.

    ax : matplotlib.pyplot.axis
        Axis instance on which to plot histogram of probabilities.
    users_performance: pd.DataFrame
        DataFrame with user performance in long format, i.e. user ID as indices, and
        columns containing metric and values.
    vmlr_performance: pd.DataFrame
        DataFrame with performance of vm LR model; index contains "overall", and A to D
        for different AIS grades; columns contains evaluation metrics.
    colour_users : str
        HEX value for colour to use for user scatter.
    label_users : str
        Label to use for user scatter.
    title : str
        Title to add if any.
    add_legend : bool
        Flag indicating whether legend should be added.
    lower_y : float
        Lower limit of y-axis.
    """
    sns.set_theme(style="whitegrid")
    plt.rcParams.update({"font.family": "serif", "font.serif": ["Times New Roman"]})

    fontsize_legend = 16
    fontsize_ticks = 16
    fontsize_title = 20
    color_vmlr = "#cf0072"
    markers = ["v", "^", "<", "o", "D", "s", "x"]
    sns.boxplot(
        data=users_performance,
        x="metric",
        y="value",
        color="lightgray",
        saturation=0.6,
        showfliers=False,
        ax=ax,
        zorder=1,
    )
    rng = np.random.default_rng(seed=17)
    jitter_users = rng.uniform(
        low=-0.2, high=0.2, size=users_performance.index.unique().shape[0]
    )
    jitter_vmlr = rng.uniform(low=-0.2, high=0.2, size=1)
    for i, metric in enumerate(users_performance["metric"].unique()):
        for j, user in enumerate(  # pylint: disable=W0621
            users_performance.index.unique()
        ):
            ax.scatter(
                jitter_users[j] + i,
                users_performance.loc[user, "value"][
                    users_performance.loc[user, "metric"] == metric
                ],
                s=50,
                c=colour_users,
                marker=markers[j],
                zorder=2,
                label=label_users if (i == 0 and j == 0) else "",
            )
        plt.scatter(
            jitter_vmlr + i,
            vmlr_performance[metric],
            s=50,
            c=color_vmlr,
            marker=markers[-1],
            zorder=2,
            label="vM LR" if i == 0 else "",
        )
    ax.set_ylim(lower_y, 1)
    ax.set_xticks(
        ax.get_xticks(),
        ax.get_xticklabels(),
        fontsize=fontsize_ticks,
        fontfamily="serif",
    )
    ax.set_yticks(
        ax.get_yticks(),
        ax.get_yticklabels(),
        fontsize=fontsize_ticks,
        fontfamily="serif",
    )
    ax.set_xlabel(None)
    ax.set_ylabel(None)
    if add_legend:
        ax.legend(loc="lower left", fontsize=fontsize_legend)
    if title:
        ax.set_title(title, fontsize=fontsize_title, fontweight="bold")

    return ax


if __name__ == "__main__":
    ais = pd.read_csv(
        DATA_PATH / "X_values_non_summarized_AISnan_1812.csv", index_col=0
    )["AIS"]
    user_predictions = pd.read_csv(
        DATA_PATH / "user-predictions" / "user-predictions-combined.csv", index_col=0
    )[["Patientennummer", "user_id", "useranswer", "correctanswer"]]
    user_predictions.replace(to_replace={"No": 0, "Yes": 1}, inplace=True)
    user_predictions = user_predictions.join(ais, on="Patientennummer")
    performance_by_user_long = (
        _compute_performance_by_user(user_predictions).unstack().reset_index()
    )
    performance_by_user_long.rename(
        columns={"level_0": "metric", "level_1": "user", 0: "value"}, inplace=True
    )
    performance_by_user_long.set_index("user", inplace=True)
    performance_vmlr = pd.read_csv(DATA_PATH / "vmlr_performance.csv", index_col=0)

    sns.set_theme(style="whitegrid")
    fig, ax = plt.subplots(1, 1, figsize=(7, 5))
    _ = individual_boxplot(
        ax,
        performance_by_user_long,
        performance_vmlr.loc["overall", :],
        add_legend=True,
    )
    fig.tight_layout()
    fig.savefig(
        PLOTS_PATH / "performance_overall.pdf",
        transparent=True,
        dpi=200,
        format="pdf",
    )
    plt.close()

    for ais_grade in user_predictions["AIS"].unique():
        user_predictions_by_ais = user_predictions[user_predictions["AIS"] == ais_grade]
        performance_by_user_long_by_ais = (
            _compute_performance_by_user(user_predictions_by_ais)
            .unstack()
            .reset_index()
        )
        performance_by_user_long_by_ais.rename(
            columns={"level_0": "metric", "level_1": "user", 0: "value"}, inplace=True
        )
        performance_by_user_long_by_ais.set_index("user", inplace=True)
        fig, ax = plt.subplots(1, 1, figsize=(7, 5))
        _ = individual_boxplot(
            ax,
            performance_by_user_long_by_ais,
            performance_vmlr.loc[f"AIS_{ais_grade}", :],
            add_legend=True,
            title="AIS " + ais_grade,
            lower_y=0,
        )
        fig.tight_layout()
        fig.savefig(
            PLOTS_PATH / f"performance_AIS_{ais_grade}.pdf",
            transparent=True,
            dpi=200,
            format="pdf",
        )
        plt.close()
